/*
 Navicat Premium Data Transfer

 Source Server         : mysql
 Source Server Type    : MySQL
 Source Server Version : 50736
 Source Host           : 192.168.56.101:3306
 Source Schema         : blog

 Target Server Type    : MySQL
 Target Server Version : 50736
 File Encoding         : 65001

 Date: 27/09/2023 13:38:46
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for t_blog
-- ----------------------------
DROP TABLE IF EXISTS `t_blog`;
CREATE TABLE `t_blog`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `summary` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `release_date` datetime NULL DEFAULT NULL,
  `click_hit` int(255) NULL DEFAULT NULL,
  `reply_hit` int(255) NULL DEFAULT NULL,
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `keyword` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT '',
  `type_id` int(255) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_blog
-- ----------------------------
INSERT INTO `t_blog` VALUES (1, '网站预留 ', '网站预留 ', '2023-09-27 13:26:18', 0, 0, '默认 ', '', 0);
INSERT INTO `t_blog` VALUES (2, '5个优化java代码的性能的tips', '5个优化java代码的性能的tips', '2023-09-27 13:27:36', 0, 0, '大多数时候，应用性能的优化不是必需的，但是本文包含的5种办法非常简单，可以在代码开发期间低成本得采纳，以防止java程序变慢以及占用更多的资源。\r\n\r\n1.尽可能于设置HashMap和ArrayList的大小\r\n\r\n2.对HashMap的复合key使用实体类包装\r\n\r\n3.使用ThreadLocalRandom 替换 Random\r\n\r\n4.使用debug日志时，避免不必要的函数调用\r\n\r\n5.停止使用JDK8\r\n\r\n尽可能于设置HashMap和ArrayList的大小\r\nHashMap和ArrayList等基于数组的结构，都存在一个问题，就是当不断添加数据超过阈值时，就会触发resize扩容操作，尽管这些类的大多数操作都很快，比如ArrayList.get(index)是 o(1)时间复杂度，但是resize操作的时间都是O(n),并且可能会出现很多次的重复。\r\n\r\nArrayList 默认的容量为10，当数据超过10时候，就会触发扩容，扩容操作为new_capacity = old_capacity * 1.5;此时会变成15大小的数组。\r\n\r\n假如我们需要添加500个元素到ArrayList中，那么它的内部表现是这样的:10->15->22->33->74->111->167->250->375->563. 总共产生了9次不需要的扩容操作，每次操作都会复制一次整个数组，同时最后产生了一个563大小的数组，而其实我们只需要500大小的数组，这里就导致了11%空间的浪费。\r\n\r\n如果我们能明确值存储500个对象，就可以将ArrayList大小直接设定为500。\r\n\r\nini\r\n复制代码\r\nList<Object> list = new ArrayList(500);\r\n这样就可以避免使用过程中的扩容操作以及浪费的45%内存(20大小的数组，只使用了11个位置，浪费了9个)。\r\n\r\nHashMap的扩容情况比ArrayList更严重，因为它含有一个扩容因子参数，默认为0.75，当数据量达到容量的0.75时就会触发扩容。\r\n\r\n因为有扩容因子的存在，对于HashMap大小的设置会有一点麻烦，推荐使用Guava的Maps.newHashMapWithExpectedSize(int size)方法，该方法内置了预期大小的计算.\r\n\r\nini\r\n复制代码\r\nMap<String> map = Maps.newHashMapWithExpectedSize(24);\r\n对HashMap的复合key使用实体类包装\r\n当我们在HashMap中使用复合的String作为key时，使用一个实体类可以加快速度和避免内存分配。\r\n\r\n错误方式\r\n\r\nini\r\n复制代码\r\n    String[] prefixes;\r\n    String[] suffixes;\r\n    Map<String, Object> concatMap;\r\n    // 获取数据\r\n      for (int i = 0; i < prefixes.length; ++i) {\r\n        concatMap.get(prefixes[i] + \";\" + suffixes[i]);\r\n      }\r\n正确方式\r\n\r\nini\r\n复制代码\r\n    String[] prefixes;\r\n    String[] suffixes;\r\n    Map<Pair, Object> pairMap;\r\n    // 获取数据\r\n      for (int i = 0; i < prefixes.length; ++i) {\r\n        pairMap.get(new Pair(prefixes[i], suffixes[i]));\r\n      }\r\n正确方式的性能是错误方式的3.7倍。\r\n\r\n使用ThreadLocalRandom 替换 Random\r\nRandom 是java开发者非常熟悉的类，它的作用是用来生成随机数。它在功能上没有什么问题，唯一的问题是因为那部使用同步代码块，多个线程并发时，就会出现竞争、冲突问题，导致效率变慢。\r\n\r\nThreadLocalRandom 是JDK1.7中新推出的基于ThreadLocal的Radnom类，实现了每个线程拥有自己的Random类，避免了多线程时的冲突。\r\n\r\n在JDK1.7版本以后，在任何场景都应该优先使用ThreadLocalRandom,它只有好处没有坏处。\r\n\r\n使用debug日志时，避免不必要的函数调用\r\njava中最有流行的日志框架当属Slf4j,其中就提供了debug方法来记录日志。debug 方法提供了占位符来避免在非debug日志等级时的字符串拼接。\r\n\r\nlogger.debug(\"Printing variable value: {}\", variable);\r\n\r\n但是在某些使用错误的场景中，就会带来意想不到的资源浪费。比如调用了json.toString();\r\n\r\nini\r\n复制代码\r\nJSONObject json = xxx;\r\nlogger.debug(\"Printing variable value: {}\", json.toString());\r\n这种使用方法会导致json.toString()会被调用，但是最后又不输出。使用logger.isDebugEnabled可以消除该问题。\r\n\r\nini\r\n复制代码\r\nJSONObject json = xxx;\r\nif (logger.isDebugEnabled()) {\r\n    logger.debug(\"Printing variable value: {}\", json.toString());\r\n}\r\n停止使用JDK8\r\nJDK8后续的LTS版本11中，包含了非常多的优化，比如String类的优化，GC的优化等。所有的java应用都大量使用String,在JDK8中,String比较大而且慢。在JDK8中，String使用char[]进行存储。\r\n\r\narduino\r\n复制代码\r\nprivate final char[] value;\r\nJDK9以后变成了byte[];\r\n\r\narduino\r\n复制代码\r\nprivate final byte[] value;\r\n一系列存储和编码的优化减少了String存储的大小和编码解码的速度。', '', 1);

-- ----------------------------
-- Table structure for t_blogger
-- ----------------------------
DROP TABLE IF EXISTS `t_blogger`;
CREATE TABLE `t_blogger`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `profile` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `nick_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `sign` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `image_path` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_blogger
-- ----------------------------
INSERT INTO `t_blogger` VALUES (1, 'admin', 'admin', 'admin默认信息', 'admin', 'admin', NULL);

-- ----------------------------
-- Table structure for t_blogtype
-- ----------------------------
DROP TABLE IF EXISTS `t_blogtype`;
CREATE TABLE `t_blogtype`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `type_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `order_no` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_blogtype
-- ----------------------------
INSERT INTO `t_blogtype` VALUES (1, 'java', 1);
INSERT INTO `t_blogtype` VALUES (2, 'mysql', 2);
INSERT INTO `t_blogtype` VALUES (3, 'Spring', 3);

-- ----------------------------
-- Table structure for t_comment
-- ----------------------------
DROP TABLE IF EXISTS `t_comment`;
CREATE TABLE `t_comment`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_ip` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `blog_id` int(255) NULL DEFAULT NULL,
  `content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `comment_date` datetime NULL DEFAULT NULL,
  `state` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_comment
-- ----------------------------

-- ----------------------------
-- Table structure for t_link
-- ----------------------------
DROP TABLE IF EXISTS `t_link`;
CREATE TABLE `t_link`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `link_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `link_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `order_no` int(11) NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_link
-- ----------------------------
INSERT INTO `t_link` VALUES (1, 'github', 'https://www.github.com', 1);

SET FOREIGN_KEY_CHECKS = 1;
