<%-- css , js 等静态资源 --%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%
    String path = request.getScheme() + "://" + request.getServerName() + ":" + request.getServerPort() + request.getContextPath() + "/";

%>
<!-- 最新版本的 Bootstrap 核心 CSS 文件 -->
<link rel="stylesheet" href="<%=path%>static/bootstrap/css/bootstrap.min.css">
<!-- 导入依赖的jquery文件 -->
<script src="<%=path%>static/bootstrap/js/jquery-1.11.2.min.js"></script>
<!-- 最新的 Bootstrap 核心 JavaScript 文件 -->
<script src="<%=path%>static/bootstrap/js/bootstrap.min.js"></script>